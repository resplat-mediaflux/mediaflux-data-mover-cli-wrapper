help([==[

Description
===========
mediaflux-data-mover is a data transfer tool for uploading and/or downloading data from a Mediaflux server using a shareable token.

More information
================
 - Homepage: https://wiki-rcs.unimelb.edu.au/display/RCS/Data+Mover
]==])

whatis([==[Description: mediaflux-data-mover is a data transfer tool for uploading and/or downloading data from a Mediaflux server using a shareable token.]==])
whatis([==[Homepage: https://wiki-rcs.unimelb.edu.au/display/RCS/Data+Mover]==])

local root = "/data/gpfs/admin/mediaflux-data-mover/current/"

conflict("mediaflux-data-mover")

prepend_path("PATH", pathJoin(root, "bin"))
