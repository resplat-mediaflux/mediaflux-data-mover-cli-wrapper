@echo off

REM This script should be installed into the directory contains 'Mediaflux Data Mover.exe'

pushd %~dp0
set DM_HOME=%cd%
popd

set DM_JAR=%DM_HOME%\app\data-mover.jar
set JAVA_HOME=%DM_HOME%\runtime
set PATH=%JAVA_HOME%\bin;%PATH%
set JAVA=%JAVA_HOME%\bin\java

if "%~1"=="" (
    "%JAVA%" -help
) else (
    "%JAVA%" --illegal-access=deny --add-opens java.base/jave.util.zip=ALL-UNNAMED -jar "%DM_JAR%" %*
)